# Docker Socat Ipv6 Supervisor


* supervisor logs under `/var/log/ssl-socat-stderr.log` and `/var/log/ssl-socat-stdout.log`
* connection logs under: /var/log/socat.ipv6.$port.$(date +%F).log
* will clean logs after 14 days
*  has to be installed under /etc/custom/docker-socat-ipv6-supervisor ( see below )
INSTALLING: 

> ## 1. Prepare system
> | alpine  | debian/ubuntu | RHEL | 
> |---|---|---|
> | `apk add git bash supervisor` | `apt install supervisor bash git` | `yum install supervisor bash git` | 

> ## 2. Clone and Setup
> ```
> git clone https://gitlab.com/the-foundation/docker-socat-ipv6-supervisor /etc/custom/docker-socat-ipv6-supervisor
> ln -s  /etc/custom/docker-socat-ipv6-supervisor/ipv6-socat-443.conf /etc/supervisor/conf.d/
> service supervisor start
> supervisorctl reload
> ```
