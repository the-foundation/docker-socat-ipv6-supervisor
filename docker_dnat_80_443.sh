#!/bin/bash
echo >  /dev/shm/docker-nginx-bind-direct.log
logger()  { echo "$(date)" "$@" >> /dev/shm/docker-nginx-bind-direct.log ; } ;
logpipe() { cat >> /dev/shm/docker-nginx-bind-direct.log                 ; } ;

## check for ipv6
ipv6_jq=$(docker inspect nginx|jq -c .[].NetworkSettings.Networks|jq -c "[to_entries[] | {id: .key} + .value]"|jq .[].GlobalIPv6Address|cut -d'"' -f2)
ipv6_ready=yes;
[[ -z $ipv6_jq ]] && { ipv6_ready=no ; logger "ipv6 address empty" ; } ;
(ping6 $ipv6_jq   -c3 2>&1|grep  -e "bytes from"|wc -l )|grep -q ^0$ &&  { ipv6_ready=no ; logger "ipv6 no ping to $ipv6_jq" ; } ;
ip6tables -L -v -n -t nat 2>&1 |grep  -e "Table does not exist" -e 'can.t initialize ip6tables table `nat' -q && { ipv6_ready=no ; logger "ipv6 no iptables -t nat" ; } ;
curl -kv http://[$ipv6_jq] 2>&1 |grep -e "< HTTP/" -e "< Content-" -q || { ipv6_ready=no ; logger "ipv6 curl failed failed to http://[$ipv6_jq]" ; } ;

## check for ipv4
ipv4_jq=$(docker inspect nginx|jq -c '.[].NetworkSettings.Networks["nginx-proxy"].IPAddress'|cut -d'"' -f2)
ipv4_ready=yes;

[[ -z $ipv4_jq ]] && { ipv4_ready=no ; logger "ipv4 address empty" ; } ;
(ping $ipv4_jq   -c3 2>&1|grep  -e "bytes from"|wc -l )|grep -q ^0$ && { ipv4_ready=no ; logger "ipv4 no ping to $ipv6_jq" ; } ;
iptables -L -v -n -t nat 2>&1 |grep  -e "Table does not exist" -e 'can.t initialize iptables table `nat' -q && { ipv4_ready=no ; logger "ipv4 no iptables -t nat" ; } ;
curl -kv http://$ipv4_jq 2>&1 |grep -e "< HTTP/" -e "< Content-" -q || { ipv4_ready=no ; logger "ipv4 curl failed failed to http://$ipv4_jq" ; } ;


#####
##   DNAT IPV4
#####

[[ "$ipv4_ready" = "yes" ]] || logger "ipv4 not ready"
[[ "$ipv4_ready" = "yes" ]] && {
ipv4_addresses=$(ip -4  a s |grep -v -e "inet 169.254.9.229" -e "inet 127.0.0.1" |grep "inet "| sed 's/.\+inet //g'|cut -d"/" -f1|cut -d" " -f1)
logger ipv4 DNAT for $(echo $ipv4_addresses)

[[ "REDIRECT_LINK_LOCAL" = "true" ]] || ipv4_addresses=$(echo "$ipv4_addresses" |grep -v fe80)

## check forward to host rules and add if they are  missing
logger forward
( iptables -L FORWARD -v -n 2>&1 |grep "$ipv4_jq"|grep "ACCEPT"|grep "multiport"|grep 80|grep 443 || iptables -I FORWARD -p tcp -m multiport --ports 80,443 -d "$ipv4_jq" -j ACCEPT        ) 2>&1|logpipe
logger related
( iptables -L FORWARD -v -n 2>&1 |grep ACCEPT|grep ::/0|grep RELATED|grep ESTABL|| iptables -I FORWARD -m state --state RELATED,ESTABLISHED -j ACCEPT  2>&1                                 ) 2>&1|logpipe
logger config
(
echo '###NGINX ipv4_redir'
for address in ${ipv4_addresses}; do
iptables -L PREROUTING  -t nat -v -n|grep DNAT  |grep $ipv4_jq|grep 80|grep 443|grep -q ${address}||   echo iptables -I PREROUTING  -t nat -p tcp -m multiport --ports 80,443  -d ${address} -j DNAT --to  $ipv4_jq
iptables -L POSTROUTING -t nat -v -n|grep ACCEPT|grep $ipv4_jq|grep 80|grep 443|grep -q ${address}||   echo iptables -I POSTROUTING -t nat -p tcp -m multiport --ports 80,443  -d $ipv4_jq -j ACCEPT

done
) > /dev/shm/.nginx-iptables.new
logger  "GENERATED::"
cat /dev/shm/.nginx-iptables.new |logpipe

logger update
test -e /dev/shm/.nginx-iptables && logger have previous rules
test -e /dev/shm/.nginx-iptables || logger no previous iptables
test -e /dev/shm/.nginx-iptables || ( mv /dev/shm/.nginx-iptables.new /dev/shm/.nginx-iptables;bash /dev/shm/.nginx-iptables ) 
test -e /dev/shm/.nginx-iptables && {
                                 changed=no
                                 [[ "$(md5sum /dev/shm/.nginx-iptables)" = "$(md5sum /dev/shm/.nginx-iptables.new)" ]]  || changed=yes
                                 [[ "$changed" = "yes" ]] || logger "no change detcted"
                                 [[ "$changed" = "yes" ]] && {
                                 logger change detected ;
                                 test -e /dev/shm/.nginx-iptables && {
                                         sed 's/tables -I/tables -D/g' /dev/shm/.nginx-iptables  > /dev/shm/.nginx-iptables.delete
                                         logger "deleting:" $(cat /dev/shm/.nginx-iptables.delete|sed 's/$/|/'|tr -d '\n')
                                         bash  /dev/shm/.nginx-iptables.delete 2>&1|logpipe
                                         mv /dev/shm/.nginx-iptables.new /dev/shm/.nginx-iptables
                                         bash /dev/shm/.nginx-iptables 2>&1|logpipe
                                         echo -n  ; } ;
                                 echo -n  ; } ;
    echo -n ; } ;

echo -n ; } ; 


#####
##   DNAT IPV6
#####
[[ "$ipv6_ready" = "yes" ]] || logger "ipv6 not ready"
[[ "$ipv6_ready" = "yes" ]] && {
logger ipv6 DNAT for $(echo $ipv6_addresses)

ip6_addresses=$(ip -6 a s  |grep "inet6 "|grep -v "inet6 ::1/" | sed 's/.\+inet6 //g'|cut -d"/" -f1)
[[ "REDIRECT_LINK_LOCAL" = "true" ]] || ip6_addresses=$(echo "$ip6_addresses" |grep -v fe80)

## check forward to host rules and add if they are  missing
logger forward
( ip6tables -L FORWARD -v -n 2>&1 |grep "$ipv6_jq"|grep "ACCEPT"|grep "multiport"|grep 80|grep 443|| ip6tables -I FORWARD -p tcp -m multiport --ports 80,443 -d "$ipv6_jq" -j ACCEPT  ) 2>&1|logpipe
logger related
( ip6tables -L FORWARD -v -n 2>&1 |grep ACCEPT|grep ::/0|grep RELATED|grep ESTABL|| ip6tables -I FORWARD -m state --state RELATED,ESTABLISHED -j ACCEPT  2>&1 ) 2>&1|logpipe

logger config
(
echo '###NGINX ipv6_redir'
for address in ${ipv6_addresses}; do
ip6tables -L PREROUTING  -t nat -v -n|grep DNAT  |grep $ipv6_jq|grep 80|grep 443|grep -q ${address}||   echo ip6tables -I PREROUTING  -t nat -p tcp -m multiport --ports 80,443  -d ${address} -j DNAT --to  $ipv6_jq
ip6tables -L POSTROUTING -t nat -v -n|grep ACCEPT|grep $ipv6_jq|grep 80|grep 443|grep -q ${address}||   echo ip6tables -I POSTROUTING -t nat -p tcp -m multiport --ports 80,443  -d $ipv6_jq -j ACCEPT
done
) > /dev/shm/.nginx-ip6tables.new

logger  "GENERATED::"
cat /dev/shm/.nginx-ip6tables.new |logpipe

logger update
test -e /dev/shm/.nginx-ip6tables && logger have previous rules
test -e /dev/shm/.nginx-ip6tables || logger no previous iptables
test -e /dev/shm/.nginx-ip6tables || ( logger "no previous iptables" ;mv /dev/shm/.nginx-ip6tables.new /dev/shm/.nginx-ip6tables ; bash /dev/shm/.nginx-ip6tables) 
test -e /dev/shm/.nginx-ip6tables && {

                                 changed=no
                                 [[ "$(md5sum /dev/shm/.nginx-ip6tables)" = "$(md5sum /dev/shm/.nginx-ip6tables.new)" ]]  || changed=yes
                                 [[ "$changed" = "yes" ]] || logger "no change detected"
                                 [[ "$changed" = "yes" ]] && {
                                 logger change detected ;
                                 test -e /dev/shm/.nginx-ip6tables && {
                                         sed 's/tables -I/tables -D/g' /dev/shm/.nginx-ip6tables  > /dev/shm/.nginx-ip6tables.delete
                                         logger "deleting:" $(cat /dev/shm/.nginx-ip6tables.delete|sed 's/$/|/'|tr -d '\n')
                                         bash  /dev/shm/.nginx-ip6tables.delete
                                         mv /dev/shm/.nginx-ip6tables.new /dev/shm/.nginx-ip6tables
                                         bash /dev/shm/.nginx-ip6tables
                                         echo -n  ; } ;
                                         
                                 echo -n  ; } ;
    echo -n ; } ;
echo -n ; } ;

